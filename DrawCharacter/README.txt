████████████████████████████████████████████████████████████████████████████████
▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
                                                                                
                   EQ2340 Assignment 2: Character Recognition              
                                                                                
                                                                                
   SUPPLIER : Mark Wong                |   DATE : 2016.10.02                    
    CRACKER : Samiul Alam              |   DiSK : 581 KB                    
                                                                                
   PLATFORM : MATLAB                                                           
   LANGUAGE : ENGLISH                                                                                                                       
                                                                                
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
                                                                                
                            	  How to use:                       
                                                                                
                                                                                
* After calling DrawCharacter.m, the output from DrawCharacter (xybout) can be
  sent to 'Feature\ Extractor/Main.m': the feature extractor.

* Main.m uses the following files:

RemoveZeros.m
NormalizeCharacter.m
CreateGrid.m
PlotHistogram.m

* The resulting feature vector is stored in the variable n.

* The folder 'DrawCharacter/Plots' contains plots of different characters as
  specified in the course compendium.

* The folder 'DrawCharacter/Demo' contains a file Demo.m that you can run to
  verify that pen movements between strokes don't matter.

* Enjoy!
                                                                                
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓▒░░  ░░▒▓█▓
████████████████████████████████████████████████████████████████████████████████
 ░░░ ▒ ▒ ▒▓▓▓▓▓▓█▓▓█▓█▓█████████▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓██████████████████████████▓
 ░ ░ ▒ ▒ ▒ ▓▓ ▓▓█ ▓█▓█▓ ██████ █▓▓▓▓ ▒▒▒▒▒▒ ▒▒▒▒▒▒ ▒▓█ ██████ ████ ██████ ████ ▓
 ░   ▒   ▒  ▓ ▓ █ ▓█ █▓ ██ ██  █▓ ▓▓ ▒ ▒▒▒▒ ▒ ▒ ▒▒ ▒▓█ █ ████ █ ██ █ ████ █ ██ ▓
 ░   ▒   ▒    ▓ █  █ █▓  █ █   █  ▓▓ ▒ ▒  ▒ ▒ ▒  ▒ ▒▓█ █  █ █ █ ██    █ █ █  █  
 ░            ▓ █  █           █  ▓▓ ▒ ▒  ▒ ▒ ▒    ▒▓█ █  █     ██    █   █     
              ▓    █              ▓▓ ▒ ▒  ▒ ▒       ▓  █         █    █         
                   █               ▓   ▒    ▒       ▓  █              █ 