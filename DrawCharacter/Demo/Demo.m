disp('Lets start by drawing the character X normally. Press left mouse-button to continue');
k=waitforbuttonpress;

disp('Drawing X normally...Wait until done.');
load('X_raw');
AnimateCharacter(raw_data);

disp('Done. Press left mouse-button to display its feature sequence');
k=waitforbuttonpress;

disp('Displaying feature sequence of normal X...');
Main(raw_data)

disp('Now lets look at X with wild mouse movements between strokes. Press left mouse-button');
k=waitforbuttonpress;

disp('Drawing X wild...Wait until done.');
load('Xwild_raw');
AnimateCharacter(raw_data);

disp('Done. Press left mouse-button to display its feature sequence');
k=waitforbuttonpress;

disp('Displaying feature sequence of wild X... Compare this');
Main(raw_data);
disp('Done.');